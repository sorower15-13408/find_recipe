import 'dart:math';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:connectivity/connectivity.dart';
import 'package:find_recipe/catSearch/searchByrecipe.dart';
import 'package:find_recipe/json_parse_data/json_pojo.dart';
import 'package:find_recipe/recipe_info.dart';

import 'package:find_recipe/responses.dart';
import 'package:find_recipe/search2.dart';
import 'package:find_recipe/webview.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:webview_flutter/webview_flutter.dart';

class search extends StatefulWidget {
  String id;
  search({Key key, @required this.id}) : super(key: key);
  @override
  _searchState createState() => _searchState(this.id);
}

class _searchState extends State<search> with SingleTickerProviderStateMixin {
  String id;
  _searchState(this.id);
  List<data_list> _list = [];
  List<data_list> to_display = [];
  TextEditingController _controller = TextEditingController();
  TabController _tabController;
  void _handleIndex() {
    setState(() {});
  }

  bool isoffline;

  int i = 1;
  Future<void> connectivity() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile) {
      // I am connected to a mobile network.
      setState(() {
        isoffline = true;
      });

      print("connected");
    } else if (connectivityResult == ConnectivityResult.wifi) {
      setState(() {
        isoffline = true;
      });
      // I am connected to a wifi network.
      print("connected");
    } else {
      setState(() {
        isoffline = false;
      });
      print(
          "dissconneteddsfffffffffffffffffffffffffffffffffffffffffffffffffffffffdsfsd");
    }
  }

  static const Color one = Color(0xff808000);
  static const Color two = Color(0xff608000);
  static const Color three = Color(0xff208080);
  List<Color> colors = [one, two, three];
  static final random = new Random();

  Color colorrandom() {
    return colors[random.nextInt(3)];
  }

  SharedPreferences mypref;
  Future _future;
  Future _data;
  var url = "";
  ScrollController _scrollController;
  int index;
  Widget _widget;
  String query;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _tabController = TabController(length: 2, vsync: this, initialIndex: 0);
    get_pref(id).then((value) {
      setState(() {
        query = value;
      });
    });
    setState(() {
      _list = to_display;
    });
    _controller.addListener(() {
      if (_controller.text.isEmpty) {
        setState(() {
          _list = to_display;
        });
      } else {
        setState(() {
          setState(() {
            to_display = _list.where((element) {
              var title = element.recipies.toLowerCase();
              return title.contains(_controller.text.toLowerCase());
            }).toList();
          });
        });
      }
    });

    getmoredata().then((value) {
      setState(() {
        _list = to_display = value;
      });
    });
    connectivity();
    _future = data();
    _tabController.addListener(() {
      // _widget = _tabController.index == 1
      //     ? ElevatedButton(onPressed: () {}, child: Text("Search"))
      //     : Container();
      handle();
    });
  }

  bool _isclicked = false;
  Widget handle() {
    setState(() {});
    if (_tabController.index == 1)
      return TextButton(
          style: ButtonStyle(
            textStyle: MaterialStateProperty.all<TextStyle>(
                TextStyle(color: Colors.greenAccent)),
          ),
          onPressed: () {
            setState(() {
              _isclicked = true;
            });
          },
          child: Text(
            "Search",
            // style: TextStyle(color: Colors.greenAccent),
          ));
    else
      return Container();
  }

  webview() {
    return WebView(
      initialUrl: "https://www.google.com/search?q=${_controller.text + query}",
      javascriptMode: JavascriptMode.unrestricted,
    );
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          actions: [
            SizedBox(child: handle()),
            IconButton(
              icon: Icon(Icons.add_circle),
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => search2(id: id)));
              },
            ),
          ],
          backgroundColor: Colors.white,
          title: Container(
            height: 50,
            child: TextField(
              controller: _controller,
              decoration: InputDecoration(
                  prefixIcon: Icon(Icons.search),
                  border: OutlineInputBorder(),
                  hintText: "Serach Recipes",
                  contentPadding: EdgeInsets.all(8)),
            ),
          ),
          bottom: TabBar(
            // indicatorColor: Color(0xFFFFFD2D2),
            controller: _tabController,
            indicator: BoxDecoration(color: Colors.grey[300]),
            indicatorWeight: 2,
            unselectedLabelColor: Colors.grey,
            indicatorColor: Colors.white,

            tabs: [
              Container(
                height: 40,
                child: Center(
                  child: Text(
                    "App Results",
                  ),
                ),
              ),
              Container(
                  child: Text(
                "Google Results",
              )),
            ],
          ),
        ),
        body: TabBarView(
          controller: _tabController,
          children: [
            searchByrecipe(
              id,
            ),
            googleSearch(),
          ],
        ),
      ),
    );
  }

  Widget googleSearch() {
    if (_controller.text.isNotEmpty) {
      if (_isclicked == true) {
        setState(() {
          _isclicked = false;
        });
        return webview();
      }
    }
    return Container();
  }

  Widget searchByrecipe(id) {
    return Column(mainAxisSize: MainAxisSize.min, children: [
      // Padding(
      //     padding: EdgeInsets.fromLTRB(20, 65, 20, 20),
      //     child: Row(
      //       children: [
      //         Expanded(
      //           child: Material(
      //             borderRadius: BorderRadius.all(Radius.circular(50)),
      //             shadowColor: Colors.grey[350],
      //             elevation: 2,
      //             child: TextField(
      //               controller: _controller,
      //               onChanged: (text) {
      //                 text = text.toLowerCase();

      //                 setState(() {
      //                   to_display = _list.where((element) {
      //                     var title = element.recipies.toLowerCase();
      //                     return title.contains(text);
      //                   }).toList();
      //                 });
      //               },
      //               cursorWidth: 1.0,
      //               decoration: InputDecoration(
      //                   hintText: "Search recipes",
      //                   contentPadding: EdgeInsets.all(10),
      //                   suffixIcon: _controller.text.isNotEmpty
      //                       ? IconButton(
      //                           splashRadius: 20,
      //                           icon: Icon(Icons.close),
      //                           onPressed: () {
      //                             setState(() {
      //                               _controller.clear();
      //                             });
      //                           })
      //                       : null,
      //                   prefixIcon: IconButton(
      //                       splashRadius: 20,
      //                       padding: EdgeInsets.only(left: 10),
      //                       icon: Icon(Icons.arrow_back_ios),
      //                       onPressed: () => Navigator.pop(context)),
      //                   enabledBorder: OutlineInputBorder(
      //                     borderRadius: BorderRadius.all(Radius.circular(50)),
      //                     borderSide: BorderSide(
      //                       color: Colors.grey[300],
      //                     ),
      //                   ),
      //                   focusedBorder: OutlineInputBorder(
      //                       borderRadius: BorderRadius.all(Radius.circular(50)),
      //                       borderSide: BorderSide(color: Colors.grey[350]))),
      //             ),
      //           ),
      //         ),
      //         IconButton(
      //             icon: Icon(Icons.tune),
      //             splashRadius: 20,
      //             onPressed: () {
      //               Navigator.push(
      //                   context,
      //                   MaterialPageRoute(
      //                       builder: (context) => search2(id: id)));
      //             }),
      //       ],
      //     )),
      Expanded(
          child: isoffline != false
              ? _controller.text.isEmpty
                  ? Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.search,
                          size: 50,
                          color: Colors.grey[400],
                        ),
                        Text("Search recipes")
                      ],
                    )
                  : to_display.length != 0
                      ? FutureBuilder(
                          future: _future,
                          builder: (context, snapshot) {
                            if (snapshot.hasData) {
                              return new GridView.builder(
                                gridDelegate:
                                    new SliverGridDelegateWithFixedCrossAxisCount(
                                  crossAxisCount: 2,
                                  childAspectRatio: 1,
                                  crossAxisSpacing: 5,
                                  mainAxisSpacing: 5,
                                ),
                                physics: BouncingScrollPhysics(),
                                itemCount: to_display.length,
                                itemBuilder: (BuildContext context, int index) {
                                  return Card(
                                    elevation: 5,
                                    shadowColor: Colors.grey[400],
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(8)),
                                    child: InkWell(
                                      onTap: () async {
                                        mypref = await SharedPreferences
                                            .getInstance();
                                        mypref.setString("title",
                                            to_display[index].recipies);

                                        var response = await post_history_data(
                                            to_display[index].recipeID, id);

                                        if (response['response'] == true) {
                                          print("success");
                                        }
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    recipe_info(
                                                      RecipeID:
                                                          to_display[index]
                                                              .recipeID
                                                              .toString(),
                                                      url: to_display[index]
                                                          .imageLink,
                                                      descrip: to_display[index]
                                                          .description,
                                                      title: to_display[index]
                                                          .recipies,
                                                      by: to_display[index].by,
                                                      ingredients:
                                                          to_display[index]
                                                              .ingredients,
                                                      time: to_display[index]
                                                          .requiredTime
                                                          .toString(),
                                                      serve: to_display[index]
                                                          .servings
                                                          .toString(),
                                                      direct: to_display[index]
                                                          .directions,
                                                      energy: to_display[index]
                                                          .energy,
                                                      protein: to_display[index]
                                                          .protein,
                                                      carbs: to_display[index]
                                                          .carbs,
                                                      fiber: to_display[index]
                                                          .fiber,
                                                      fat:
                                                          to_display[index].fat,
                                                      cholestrol:
                                                          to_display[index]
                                                              .cholestrol,
                                                      sodium: to_display[index]
                                                          .sodium,
                                                    )));
                                      },
                                      child: Container(
                                        width: 150,
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            //mostviewed
                                            ClipRRect(
                                                borderRadius: BorderRadius.only(
                                                    topLeft: Radius.circular(8),
                                                    topRight:
                                                        Radius.circular(8)),
                                                child: Container(
                                                  height: 100,
                                                  width: MediaQuery.of(context)
                                                      .size
                                                      .width,
                                                  color: colorrandom(),
                                                  child: CachedNetworkImage(
                                                    imageUrl: to_display[index]
                                                        .imageLink,
                                                    fit: BoxFit.fill,
                                                  ),
                                                )),
                                            SizedBox(
                                              height: 5,
                                            ),
                                            Flexible(
                                              child: Padding(
                                                padding:
                                                    EdgeInsets.only(left: 5),
                                                child: Text(
                                                  to_display[index].recipies,
                                                  maxLines: 2,
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      color: Colors.grey[700]),
                                                ),
                                              ),
                                            ),
                                            SizedBox(
                                              height: 10,
                                            ),
                                            Padding(
                                              padding: EdgeInsets.only(left: 5),
                                              child: Text(
                                                "By ${to_display[index].by}",
                                                style: TextStyle(
                                                    color: Colors.grey[700]),
                                              ),
                                            ),
                                            SizedBox(
                                              height: 10,
                                            ),

                                            Padding(
                                              padding: EdgeInsets.only(left: 5),
                                              child: Text(
                                                "Cooking Time: ${to_display[index].requiredTime} min",
                                                style: TextStyle(
                                                    color: Colors.grey[700]),
                                              ),
                                            ),
                                            SizedBox(
                                              height: 5,
                                            )
                                          ],
                                        ),
                                      ),
                                    ),
                                  );
                                },
                              );
                            } else {
                              return Center(
                                child: Text("error"),
                              );
                            }
                          })
                      : Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                              Text(
                                "Oops!",
                                style: TextStyle(
                                    fontSize: 15,
                                    color: Colors.grey,
                                    fontWeight: FontWeight.bold),
                              ),
                              Center(
                                child: Text(
                                  "We could not understand what you mean, try repharasing the query.",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(color: Colors.grey),
                                ),
                              )
                            ])
              : Center(child: Text("No internet")))
    ]);
  }
}
