import 'dart:math';

import 'package:autocomplete_textfield/autocomplete_textfield.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:connectivity/connectivity.dart';
import 'package:find_recipe/ing_list.dart';
import 'package:find_recipe/json_parse_data/json_pojo.dart';
import 'package:find_recipe/main_page.dart';
import 'package:find_recipe/recipe_info.dart';
import 'package:find_recipe/responses.dart';
import 'package:find_recipe/search.dart';
import 'package:find_recipe/suggestion_data.dart';
import 'package:flutter/material.dart';
import 'package:flutter_tags/flutter_tags.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';
import 'package:find_recipe/serach_result.dart';

class search2 extends StatefulWidget {
  String id;
  search2({Key key, @required this.id}) : super(key: key);
  @override
  _search2State createState() => _search2State(this.id);
}

class _search2State extends State<search2> {
  String id;
  _search2State(this.id);
  final _tagState = GlobalKey<TagsState>();
  GlobalKey<AutoCompleteTextFieldState<String>> key = new GlobalKey();
  bool isLoading = false;
  final _controller = TextEditingController();
  List _items = new List();
  SimpleAutoCompleteTextField textField;
  bool showWhichErrorText = false;

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  SharedPreferences mypref;
  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;
    return Scaffold(
        key: _scaffoldKey,
        body: SingleChildScrollView(
          child: Container(
            height: height,
            width: width,
            child: Column(
              children: [
                SizedBox(
                  height: height * 0.25,
                  child: Row(children: [
                    Expanded(
                      child: Padding(
                        padding: EdgeInsets.all(20),
                        child: AutoCompleteTextField(
                          clearOnSubmit: true,
                          controller: _controller,
                          decoration: InputDecoration(
                              suffixIcon: Icon(Icons.add_box_rounded),
                              border: new OutlineInputBorder(),
                              labelText: "Add your ingredients"),
                          suggestions: inglist,
                          itemBuilder: (context, suggestion) {
                            return ListTile(
                              title: Text(suggestion),
                              trailing: Icon(Icons.add_location),
                            );
                          },
                          itemSorter: (a, b) {
                            return a.compareTo(b);
                          },
                          itemFilter: (suggestion, query) {
                            return suggestion
                                .toLowerCase()
                                .startsWith(query.toLowerCase());
                          },
                          itemSubmitted: (data) {
                            _items.add(Item(title: data));
                          },
                          key: key,
                        ),
                      ),
                    ),
                    IconButton(icon: Icon(Icons.ac_unit), onPressed: () {})
                  ]),
                ),
                SizedBox(
                  height: 20,
                ),
                Expanded(
                  child: SingleChildScrollView(
                    child: Tags(
                      key: _tagState,
                      itemCount: _items.length,
                      itemBuilder: (int index) {
                        final item = _items[index];
                        return ItemTags(
                          activeColor: Colors.white,
                          index: index,
                          title: item.title,
                          textActiveColor: Colors.black,
                          textStyle: TextStyle(
                              fontSize: 14, fontWeight: FontWeight.bold),
                          combine: ItemTagsCombine.withTextBefore,
                          onPressed: (item) => print(item),
                          onLongPressed: (item) => print(item),
                          removeButton: ItemTagsRemoveButton(
                            onRemoved: () {
                              setState(() {
                                _items.removeAt(index);
                              });
                              return true;
                            },
                          ),
                        );
                      },
                    ),
                  ),
                ),

                SizedBox(
                  width: width,
                  height: 50,
                  child: RaisedButton(
                    color: Colors.greenAccent[400],
                    child: Text(
                      "Search",
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                    ),
                    textColor: Colors.white,
                    onPressed: () {
                      if (_items.isNotEmpty) {
                        List mylist = [];
                        for (int i = 0; i < _items.length; i++) {
                          mylist.add(_items[i].title);
                        }
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) =>
                                    serach_res(ing_list: mylist, id: id)));
                      } else {
                        _scaffoldKey.currentState.showSnackBar(SnackBar(
                          content: Text("Empty"),
                          backgroundColor: Colors.red,
                          action: SnackBarAction(
                            label: "ok",
                            textColor: Colors.white,
                            onPressed: () {
                              _scaffoldKey.currentState.hideCurrentSnackBar();
                            },
                          ),
                        ));
                      }
                    },
                  ),
                )
                // SizedBox(
                //   width: MediaQuery.of(context).size.width,
                //   height: 50,
                //   child: isLoading == false
                //       ? new RaisedButton(
                //           color: Colors.greenAccent,
                //           onPressed: () async {
                //             setState(() {
                //               isLoading = true;
                //             });

                //             if (_items.isEmpty) {
                //               setState(() {
                //                 isLoading = false;
                //               });
                //               _scaffoldKey.currentState.showSnackBar(SnackBar(
                //                 backgroundColor: Colors.red,
                //                 action: SnackBarAction(
                //                   label: "ok",
                //                   textColor: Colors.white,
                //                   onPressed: () {
                //                     _scaffoldKey.currentState
                //                         .hideCurrentSnackBar();
                //                   },
                //                 ),
                //                 content: Text(
                //                     "You can't procced with empty preferences"),
                //                 duration: Duration(seconds: 2),
                //               ));
                //             } else {
                //               List mylist = [];
                //               for (int i = 0; i < _items.length; i++) {
                //                 mylist.add(_items[i].title);
                //               }
                //               SharedPreferences pref =
                //                   await SharedPreferences.getInstance();
                //               var response = await signup3(id, mylist);
                //               if (response["response"] == true) {
                //                 print("sucess");
                //                 setState(() {
                //                   isLoading = false;
                //                   pref.setBool("islogin", true);
                //                   pref.setString("id", id);
                //                   Navigator.pushAndRemoveUntil(
                //                       context,
                //                       MaterialPageRoute(
                //                           builder: (context) => my_page()),
                //                       (route) => false);
                //                 });
                //               } else {
                //                 setState(() {
                //                   isLoading = false;
                //                 });
                //                 _scaffoldKey.currentState.showSnackBar(SnackBar(
                //                   content: Text(
                //                       "Some thing wents wrong pleas try again"),
                //                   action: SnackBarAction(
                //                     label: "ok",
                //                     onPressed: () {
                //                       _scaffoldKey.currentState
                //                           .hideCurrentSnackBar();
                //                     },
                //                   ),
                //                 ));
                //               }
                //               print(mylist);
                //             }
                //           },
                //           child: new Text(
                //             "Lets go!",
                //             style: TextStyle(
                //               fontWeight: FontWeight.bold,
                //               fontSize: 20,
                //             ),
                //           ),
                //         )
                //       : Center(
                //           child: CircularProgressIndicator(),
                //         ),
                // ),
              ],
            ),
          ),
        ));
  }
}
