import 'package:dotted_line/dotted_line.dart';
import 'package:flutter/material.dart';

class nutration extends StatefulWidget {
  String time;
  String serve;
  String energy;
  String protein;
  String carbs;
  String fiber;
  String fat;
  //
  String cholestrol;
  String sodium;
  nutration(
      {Key key,
      @required this.time,
      @required this.serve,
      @required this.energy,
      @required this.protein,
      @required this.carbs,
      @required this.fiber,
      @required this.fat,
      @required this.cholestrol,
      @required this.sodium})
      : super(key: key);
  @override
  _nutrationState createState() => _nutrationState(
      this.time,
      this.serve,
      this.energy,
      this.protein,
      this.carbs,
      this.fiber,
      this.fat,
      this.cholestrol,
      this.sodium);
}

class _nutrationState extends State<nutration> {
  String time;
  String serve;
  String energy;
  String protein;
  String carbs;
  String fiber;
  String fat;
  //
  String cholestrol;
  String sodium;
  _nutrationState(this.time, this.serve, this.energy, this.protein, this.carbs,
      this.fiber, this.fat, this.cholestrol, this.sodium);
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: EdgeInsets.only(top: 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Text("Nutrations",
                  style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                  )),
              Text("Ready In: ${time} min",
                  style: TextStyle(
                    fontSize: 16,
                    // fontWeight: FontWeight.bold,
                  )),
              Text("Serves: ${serve}",
                  style: TextStyle(
                    fontSize: 16,
                    // fontWeight: FontWeight.bold,
                  )),
            ],
          ),
        ),
        Padding(
          padding: EdgeInsets.all(30),
          child: Container(
            width: MediaQuery.of(context).size.width,
            height: 500,
            decoration: BoxDecoration(
                border: Border.all(color: Colors.black, width: 2)),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                    padding: EdgeInsets.only(left: 25, top: 25, bottom: 10),
                    child: Text(
                      "NUTRITION INFO",
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                    )),
                // Padding(
                //     padding: EdgeInsets.only(left: 25),
                //     child: Text(
                //       "Serving Size:",
                //       style:
                //           TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                //     )),
                Padding(
                    padding: EdgeInsets.only(left: 25),
                    child: Text(
                      "Serving Per Recipe : ${serve}",
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                    )),
                SizedBox(
                  height: 10,
                ),
                Divider(
                  color: Colors.black,
                  thickness: 3,
                ),
                Padding(
                  padding: EdgeInsets.all(10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "AMT. PER SERVING",
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      Text(
                        "VALUE",
                        style: TextStyle(fontWeight: FontWeight.bold),
                      )
                    ],
                  ),
                ),
                Divider(color: Colors.black, thickness: 3),

                Padding(
                  padding: EdgeInsets.only(left: 10, right: 10, top: 10),
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text("Calories ",
                            style: TextStyle(fontWeight: FontWeight.bold)),
                        Padding(
                            padding: EdgeInsets.only(left: 15),
                            child: Text(energy,
                                style: TextStyle(fontWeight: FontWeight.bold)))
                      ]),
                ),
                // Padding(
                //     padding: EdgeInsets.only(top: 10, left: 25),
                //     child: Text(
                //       "Calories from Fat 69 g",
                //     )),
                Padding(
                    padding: EdgeInsets.only(left: 10, right: 10, top: 5),
                    child: Divider(
                      color: Colors.black,
                      thickness: 1,
                    )),

                Padding(
                  padding: EdgeInsets.only(left: 10, right: 10, top: 10),
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text("Fat ",
                            style: TextStyle(fontWeight: FontWeight.bold)),
                        Padding(
                            padding: EdgeInsets.only(left: 15),
                            child: Text(fat,
                                style: TextStyle(fontWeight: FontWeight.bold)))
                      ]),
                ),

                // Padding(
                //     padding: EdgeInsets.only(top: 5, left: 40),
                //     child: Text(
                //       "Total Fat 77 g",
                //     )),

                // Padding(
                //     padding: EdgeInsets.only(top: 5, left: 40),
                //     child: Text(
                //       "Saturated Fat 1 g",
                //     )),
                Padding(
                    padding: EdgeInsets.only(left: 10, right: 10, top: 5),
                    child: Divider(
                      color: Colors.black,
                      thickness: 1,
                    )),

                Padding(
                  padding: EdgeInsets.only(left: 10, right: 10, top: 10),
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text("Cholestrol ",
                            style: TextStyle(fontWeight: FontWeight.bold)),
                        Padding(
                            padding: EdgeInsets.only(left: 15),
                            child: Text(cholestrol,
                                style: TextStyle(fontWeight: FontWeight.bold)))
                      ]),
                ),
                Padding(
                    padding: EdgeInsets.only(left: 10, right: 10, top: 5),
                    child: Divider(
                      color: Colors.black,
                      thickness: 1,
                    )),
                Padding(
                  padding: EdgeInsets.only(left: 10, right: 10, top: 10),
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text("Sodium ",
                            style: TextStyle(fontWeight: FontWeight.bold)),
                        Padding(
                            padding: EdgeInsets.only(left: 15),
                            child: Text(sodium,
                                style: TextStyle(fontWeight: FontWeight.bold)))
                      ]),
                ),

                Padding(
                    padding: EdgeInsets.only(left: 10, right: 10, top: 5),
                    child: Divider(
                      color: Colors.black,
                      thickness: 1,
                    )),

                Padding(
                  padding: EdgeInsets.only(left: 10, right: 10, top: 10),
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text("Carbohydrates",
                            style: TextStyle(fontWeight: FontWeight.bold)),
                        Padding(
                            padding: EdgeInsets.only(left: 15),
                            child: Text(carbs,
                                style: TextStyle(fontWeight: FontWeight.bold)))
                      ]),
                ),

                Padding(
                    padding: EdgeInsets.only(left: 10, right: 10, top: 5),
                    child: Divider(
                      color: Colors.black,
                      thickness: 1,
                    )),
                Padding(
                  padding: EdgeInsets.only(left: 10, right: 10, top: 10),
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text("Fiber",
                            style: TextStyle(fontWeight: FontWeight.bold)),
                        Padding(
                            padding: EdgeInsets.only(left: 15),
                            child: Text(fiber,
                                style: TextStyle(fontWeight: FontWeight.bold)))
                      ]),
                ),
                // Padding(
                //   padding: EdgeInsets.only(left: 40, top: 5),
                //   child: DottedLine(
                //     dashLength: 2,
                //     dashGapLength: 2,
                //     lineThickness: 1,
                //   ),
                // ),
                // Padding(
                //     padding: EdgeInsets.only(top: 5, left: 40),
                //     child: Text(
                //       "Sugars 61 g",
                //     )),
                Padding(
                    padding: EdgeInsets.only(left: 10, right: 10, top: 5),
                    child: Divider(
                      color: Colors.black,
                      thickness: 1,
                    )),
                Padding(
                  padding: EdgeInsets.only(left: 10, right: 10, top: 10),
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text("Protein",
                            style: TextStyle(fontWeight: FontWeight.bold)),
                        Padding(
                            padding: EdgeInsets.only(left: 15),
                            child: Text(protein,
                                style: TextStyle(fontWeight: FontWeight.bold)))
                      ]),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}

class OpenPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {}

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => true;
}
